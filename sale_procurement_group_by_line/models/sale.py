# -*- coding: utf-8 -*-
#
#
#    Author: Guewen Baconnier, Yannick Vaucher
#    Copyright 2013-2015 Camptocamp SA
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
from datetime import timedelta

from odoo import models, api, fields, osv
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.model
    def _prepare_procurement_group_by_line(self, line):
        """ Hook to be able to use line data on procurement group """
        return self._prepare_procurement_group()



    ###
    # OVERRIDE to use sale.order.line's procurement_group_id from lines
    ###
    # @api.depends('order_line.procurement_group_id.procurement_ids.state')
    # def _get_shipped(self):
    #     """ As procurement is per sale line basis, we check each line
    #
    #         If at least a line has no procurement group defined, it means it
    #         isn't shipped yet.
    #
    #         Only when all procurement are done or cancelled, we consider
    #         the sale order as being shipped.
    #
    #         And if there is no line, we have nothing to ship, thus it isn't
    #         shipped.
    #
    #     """
    #     if not self.order_line:
    #         self.shipped = False
    #         return
    #
    #     # keep empty groups
    #     groups = set([line.procurement_group_id
    #                   for line in self.order_line
    #                   if line.product_id.type != 'service'])
    #     is_shipped = True
    #     for group in groups:
    #         if not group or not group.procurement_ids:
    #             is_shipped = False
    #             break
    #         is_shipped &= all([proc.state in ['cancel', 'done']
    #                            for proc in group.procurement_ids])
    #     self.shipped = is_shipped
    #
    # ###
    # # OVERRIDE to find sale.order.line's picking
    # ###
    # def _get_pickings(self):
    #     res = {}
    #     for sale in self:
    #         group_ids = set([line.procurement_group_id.id
    #                          for line in sale.order_line
    #                          if line.procurement_group_id])
    #         if not any(group_ids):
    #             sale.picking_ids = []
    #             continue
    #         sale.picking_ids = self.env.get('stock.picking').search([('group_id', 'in', list(group_ids))])
    #
    # picking_ids = fields.One2many('stock.picking', string='Picking associated to this sale', compute='_get_pickings')
    # shipped     = fields.Boolean(
    #     compute='_get_shipped',
    #     string='Delivered',
    #     store=True)


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    def _action_procurement_create(self):
        """
        Create procurements based on quantity ordered. If the quantity is increased, new
        procurements are created. If the quantity is decreased, no automated action is taken.
        """
        precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
        new_procs = self.env['procurement.order']  # Empty recordset

        groups = {}
        for line in self:
            if line.state != 'sale' or not line.product_id._need_procurement():
                continue
            qty = 0.0
            # for proc in line.procurement_ids:
            #     qty += proc.product_qty
            # if float_compare(qty, line.product_uom_qty, precision_digits=precision) >= 0:
            #     continue

            group_id = groups.get(line._get_procurement_group_key())
            if not group_id:
                vals = line.order_id._prepare_procurement_group_by_line(line)
                group_id = self.env['procurement.group'].create(vals)
                groups[line._get_procurement_group_key()] = group_id
            line.procurement_group_id = group_id

            # if not line.order_id.procurement_group_id:

            vals = line._prepare_order_line_procurement(group_id=group_id.id)
            vals['product_qty'] = line.product_uom_qty - qty
            new_proc = self.env['procurement.order'].create(vals)
            new_proc.message_post_with_view('mail.message_origin_link',
                                            values={'self': new_proc, 'origin': line.order_id},
                                            subtype_id=self.env.ref('mail.mt_note').id)
            new_procs += new_proc
        new_procs.run()
        return new_procs

    def _get_procurement_group_key(self):
        """ Return a key with priority to be used to regroup lines in multiple
        procurement groups

        """
        return (8, self.order_id.id)

    procurement_group_id = fields.Many2one(
        'procurement.group',
        'Procurement group',
        copy=False)

    def _prepare_procurement_values(self, group_id=False):
        """ Prepare specific key for moves or other components that will be created from a stock rule
        comming from a sale order line. This method could be override in order to add other custom key that could
        be used in move/po creation.
        """
        values = super(SaleOrderLine, self)._prepare_procurement_values(group_id)
        self.ensure_one()
        date_planned = self.order_id.date_order\
            + timedelta(days=self.customer_lead or 0.0) - timedelta(days=self.order_id.company_id.security_lead)
        values.update({
            'group_id': group_id,
            'sale_line_id': self.id,
            'date_planned': date_planned,
            'route_ids': self.route_id,
            'warehouse_id': self.warehouse_line_id or False,
            'partner_id': self.order_id.partner_shipping_id.id,
            'company_id': self.order_id.company_id,
        })
        for line in self.filtered("order_id.commitment_date"):
            date_planned = fields.Datetime.from_string(line.order_id.commitment_date) - timedelta(days=line.order_id.company_id.security_lead)
            values.update({
                'date_planned': fields.Datetime.to_string(date_planned),
            })
        return values
