# -*- coding: utf-8 -*-

from odoo import models, fields, api


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.model
    def _prepare_procurement_group_by_line(self, line):
        result = super(SaleOrder, self)._prepare_procurement_group_by_line(line)
        # for compatibility with sale_quotation_sourcing
        if line._get_procurement_group_key()[0] == 8:
            if line.warehouse_id:
                result['name'] += '/' + line.warehouse_id.name
        return result

    # warehouse_id_1 = fields.Many2one(
    #     'stock.warehouse',
    #     'Default Warehouse',
    #     help="If no source warehouse is selected on line, "
    #          "this warehouse is used as default. ")


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    warehouse_line_id = fields.Many2one(
        'stock.warehouse',
        'Source Warehouse',
        help="If a source warehouse is selected, "
             "it will be used to define the route. "
             "Otherwise, it will get the warehouse of "
             "the sale order")

    def _prepare_order_line_procurement(self, group_id=False):
        result = super(SaleOrderLine, self)._prepare_order_line_procurement(group_id=group_id)
        for line in self:
            if line.warehouse_line_id:
                result['warehouse_line_id'] = line.warehouse_line_id.id
        return result

    def _get_procurement_group_key(self):
        """ Return a key with priority to be used to regroup lines in multiple
        procurement groups

        """
        priority = 8
        key = super(SaleOrderLine, self)._get_procurement_group_key()
        # Check priority
        if key[0] >= priority:
            return key
        return (priority, self.warehouse_line_id.id)